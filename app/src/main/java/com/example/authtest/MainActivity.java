package com.example.authtest;

import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static final int GOOGLE_REQUEST_CODE_PICK_ACCOUNT = 1000;
    private static final int GOOGLE_REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 1001;
    private static final String GOOGLE_SCOPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile";

    private static final String GOOGLE_SHARED_PREFERENCES_TOKEN = "shared_preferences_google_token";

    @Bind(R.id.text_view_google_token)
    TextView mTextViewGoogleToken;
    @Bind(R.id.text_view_google_user_info)
    TextView mTextViewGoogleUserInfo;
    @Bind(R.id.button_login_facebook)
    LoginButton mLoginButtonFacebook;
    @Bind(R.id.text_view_facebook_token)
    TextView mTextViewFacebookToken;
    @Bind(R.id.text_view_facebook_user_info)
    TextView mTextViewFacebookUserInfo;

    private String mGoogleEmail;
    private CallbackManager mFacebookCallbackManager;
    private AccessTokenTracker mFacebookAccessTokenTracker;
    private AccessToken mFacebookAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        googleSetUp();

        facebookSetUp();
        facebookGetUserInfo();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFacebookAccessTokenTracker.stopTracking();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GOOGLE_REQUEST_CODE_PICK_ACCOUNT) {
            if (resultCode == RESULT_OK) {
                mGoogleEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                googleGetToken();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == GOOGLE_REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR && resultCode == RESULT_OK) {
            googleGetToken();
        } else {
            mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.button_login_google)
    public void onClickLogin() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null, accountTypes, false, null, null, null, null);
        startActivityForResult(intent, GOOGLE_REQUEST_CODE_PICK_ACCOUNT);
    }

    @OnClick(R.id.button_web_view)
    public void onClickWebView() {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }

    private void googleSetUp() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String token = sharedPreferences.getString(GOOGLE_SHARED_PREFERENCES_TOKEN, null);
        if (token != null)
            googleTokenNotNull(token);
    }

    private void googleGetToken() {
        new GoogleTokenTask(this, mGoogleEmail, GOOGLE_SCOPE).execute();
    }

    public void googleHandleException(final Exception e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (e instanceof GooglePlayServicesAvailabilityException) {
                    int statusCode = ((GooglePlayServicesAvailabilityException) e).getConnectionStatusCode();
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(statusCode, MainActivity.this, GOOGLE_REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                    dialog.show();
                } else if (e instanceof UserRecoverableAuthException) {
                    Intent intent = ((UserRecoverableAuthException) e).getIntent();
                    startActivityForResult(intent, GOOGLE_REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                } else if (e instanceof IOException) {
                    Toast.makeText(MainActivity.this, "IO Exception " + e.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Exception " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                Log.e("Google login exception", e.getMessage(), e);
            }
        });
    }

    public void googleTokenNull() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTextViewGoogleToken.setText("No Internet");
            }
        });
    }

    public void googleTokenNotNull(final String token) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTextViewGoogleToken.setText(token);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                sharedPreferences.edit().putString(GOOGLE_SHARED_PREFERENCES_TOKEN, token).apply();
                new GoogleUserInfoTask(MainActivity.this, token).execute();
                new GoogleTokenExpirationTask(MainActivity.this, token).execute();
            }
        });
    }

    public void googleShowUserInfo(String response) {
        mTextViewGoogleUserInfo.setText(response);
    }

    public void googleSetTokenExpiration(String response) {
        mTextViewGoogleToken.append("\n***\nExpires: " + response);
    }

    private void facebookSetUp() {
        mFacebookCallbackManager = CallbackManager.Factory.create();

        mLoginButtonFacebook.setReadPermissions("email");
        mLoginButtonFacebook.registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mFacebookAccessToken = loginResult.getAccessToken();
                facebookGetUserInfo();
            }

            @Override
            public void onCancel() {
                Toast.makeText(MainActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("FB login exception", e.getMessage(), e);
            }
        });

        mFacebookAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {
                mFacebookAccessToken = newAccessToken;
                facebookGetUserInfo();
            }
        };
        mFacebookAccessToken = AccessToken.getCurrentAccessToken();
    }

    private void facebookGetUserInfo() {
        if (mFacebookAccessToken != null) {
            mTextViewFacebookToken.setText(mFacebookAccessToken.getToken());
            mTextViewFacebookToken.append("\n***\nExpires: " + mFacebookAccessToken.getExpires());
            GraphRequest request = GraphRequest.newMeRequest(mFacebookAccessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            mTextViewFacebookUserInfo.setText(object.toString());
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email");
            request.setParameters(parameters);
            request.executeAsync();
        } else {
            mTextViewFacebookToken.setText("No token");
            mTextViewFacebookUserInfo.setText("");
        }
    }

    //Used once for generating hash for developers.facebook.com
    /*private void facebookGenerateHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.example.authtest", PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("Hash", sign);
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            Log.e("Hash", e.getMessage(), e);
        }
    }*/
}
