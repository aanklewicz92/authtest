package com.example.authtest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;

import java.io.IOException;

public class GoogleTokenTask extends AsyncTask<Void, Void, Void> {
    private MainActivity mActivity;
    private String mScope;
    private String mEmail;

    public GoogleTokenTask(MainActivity activity, String name, String scope) {
        this.mActivity = activity;
        this.mScope = scope;
        this.mEmail = name;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (checkConnection()) {
            try {
                String token = fetchToken();
                if (token != null) {
                    mActivity.googleTokenNotNull(token);
                }
            } catch (IOException e) {
                mActivity.googleHandleException(e);
            }
        } else
            mActivity.googleTokenNull();
        return null;
    }

    private String fetchToken() throws IOException {
        try {
            return GoogleAuthUtil.getToken(mActivity, mEmail, mScope);
        } catch (GoogleAuthException fatalException) {
            mActivity.googleHandleException(fatalException);
        }
        return null;
    }

    private boolean checkConnection() {
        ConnectivityManager connMgr = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
