package com.example.authtest;

import android.os.AsyncTask;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class GoogleTokenExpirationTask extends AsyncTask<Void, Void, String> {
    private final static String URL = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=";

    private MainActivity mDownloadedListener;
    private String mToken;

    public GoogleTokenExpirationTask(MainActivity mDownloadedListener, String token) {
        this.mDownloadedListener = mDownloadedListener;
        this.mToken = token;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... aVoid) {
        OkHttpClient mHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(URL + mToken).build();
        try {
            Response response = mHttpClient.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            mDownloadedListener.googleHandleException(e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(String param) {
        String message = "No data";
        try {
            JSONObject object = new JSONObject(param);
            int seconds = object.getInt("expires_in");
            message = seconds + " seconds";
        } catch (JSONException e) {
            mDownloadedListener.googleHandleException(e);
        }
        mDownloadedListener.googleSetTokenExpiration(message);
    }
}
